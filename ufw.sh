#!/bin/bash
echo ufw allow proto tcp from any to any port 22
echo ufw allow proto tcp from any to any port 1798
echo ufw allow proto tcp from any to any port 16514
echo ufw allow proto tcp from any to any port 5900:6100
echo ufw allow proto tcp from any to any port 49152:49216
echo If you have an issue with ufw while using a bridged connection, add those two lines at the end of the /etc/ufw/before.rules just before COMMIT

echo vi /etc/ufw/before.rules
echo '-A FORWARD -d 192.168.42.11 -j ACCEPT'
echo '-A FORWARD -s 192.168.42.11 -j ACCEPT'
