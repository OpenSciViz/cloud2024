#!/bin/bash
pwd
echo slightly dated openstack ubuntu 18 install
echo https://www.digitalocean.com/community/tutorials/install-openstack-ubuntu-devstack
echo 'How to Install OpenStack on Ubuntu 18.04 with DevStack Published on August 3, 2022'
echo git clone --recursive $1 --depth 1 https://git.openstack.org/openstack-dev/devstack
echo pushd devstack
touch hon_local.conf
echo '[[local|localrc]]' >> hon_local.conf

echo 'Password for KeyStone, Database, RabbitMQ and Servicea'
echo 'ADMIN_PASSWORD=aabbccddeeff >> hon_local.conf'
echo 'DATABASE_PASSWORD=$ADMIN_PASSWORD >> hon_local.conf'
echo 'RABBIT_PASSWORD=$ADMIN_PASSWORD >> hon_local.conf'
echo 'SERVICE_PASSWORD=$ADMIN_PASSWORD >> hon_local.conf'

echo 'Host IP - get your Server/VM IP address from ip addr command'
IPv4=`ip a|grep 'scope global'|awk '{print $2}'|cut -d'/' -f1`
echo "HOST_IP=$IPv4 >> hon_local.conf"
echo 'ln -a hon_local.conf local.conf'
echo './stack.sh'
echo 'from digitalocean...'
echo 'The following features will be installed:'
echo 'Horizon – OpenStack Dashboard'
echo 'Nova – Compute Service'
echo 'Glance – Image Service'
echo 'Neutron – Network Service'
echo 'Keystone – Identity Service'
echo 'Cinder – Block Storage Service'
echo 'Placement – Placement API'
echo "access OpenStack via a web browser browse your Ubuntu IP address -- https://${IPv4}/dashboard"

