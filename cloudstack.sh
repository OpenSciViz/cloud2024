#!/bin/bash
echo apt update
echo apt upgrade
echo apt autoremove
echo https://docs.cloudstack.apache.org/en/latest/installguide/configuration.html
echo https://docs.cloudstack.apache.org/en/latest/installguide/hypervisor/kvm.html
echo 'Optional SELinux policy admin -- can cause breakage'
echo apt install policycoreutils-python-utils
echo
echo https://community.linuxmint.com/tutorial/view/458
echo
echo apt install bridge-utils net-tools
echo apt install qemu-kvm libvirt-daemon libvirt-daemon-system libvirt-clients chrony openntpd
echo https://docs.cloudstack.apache.org/en/latest/installguide/management-server/index.html#install-mgt
echo Set the server-id according to your database setup.
echo
echo apt install mysql-server mysql-client python3-mysqldb
echo
echo edit [mysqld] section after datadir of /etc/my.cnf or /etc/mysql/my.cnf
echo server-id=source-01
echo innodb_rollback_on_timeout=1
echo innodb_lock_wait_timeout=600
echo max_connections=350
echo log-bin=mysql-bin
echo binlog-format = 'ROW'
echo systemctl restart mysql
echo The following command creates the “cloud” user on the database.
echo 'cloudstack-setup-databases cloud:<dbpassword>@localhost [ --deploy-as=root:<password> | --schema-only ] -'

echo
echo set up NFS shares for secondary and optionally primary storage
echo 'apt install nfs-kernel-server'
echo Uncomment the following /etc/sysconfig/nfs lines:
echo LOCKD_TCPPORT=32803
echo LOCKD_UDPPORT=32769
echo MOUNTD_PORT=892
echo RQUOTAD_PORT=875
echo STATD_PORT=662
echo STATD_OUTGOING_PORT=2020
echo
echo 'mkdir -p /export/primary && mkdir -p /export/secondary'
echo "echo '/export  *(rw,async,no_root_squash,no_subtree_check)' >> /etc/exports"
echo On the management server, create a mount point for secondary storage. For example:
echo 'mkdir -p /mnt/secondary'
echo Mount the secondary storage on your Management Server
echo 'mount -t nfs nfsservername:/export/secondary /mnt/secondary'
echo vi /etc/idmapd.conf
echo '/etc/idmapd.conf -- Remove the character # from the beginning of the Domain line in idmapd.conf and replace the value in the file with your own domain.'

echo 'Domain = domian-name aka 1percent.us or wehike.us or sciviz.biz'
echo 'service rpcbind start && service nfs start && chkconfig nfs on && chkconfig rpcbind on'
echo reboot
echo
echo On the Management Server, run the kvm cloud-install-sys-tmplt
echo '/usr/share/cloudstack-common/scripts/storage/secondary/cloud-install-sys-tmplt -m /mnt/secondary -u http://download.cloudstack.org/systemvm/4.19/systemvmtemplate-4.19.0-kvm.qcow2.bz2 -h kvm -s <optional-management-server-secret-key> -F'

echo
echo apt install cloudstack-agent cloudstack-management
echo apt install golang-github-xanzy-go-cloudstack-dev
echo add cloudstack user to /etc/sudoers ... 2 lines ...
echo 'cloudstack ALL=NOPASSWD: /usr/bin/cloudstack-setup-agent'
echo 'defaults:cloudstack !requiretty'
echo 'ln -s /etc/apparmor.d/usr.sbin.libvirtd /etc/apparmor.d/disable'
echo 'ln -s /etc/apparmor.d/usr.lib.libvirt.virt-aa-helper /etc/apparmor.d/disable'
echo 'apparmor_parser -R /etc/apparmor.d/usr.sbin.libvirtd'
echo 'apparmor_parser -R /etc/apparmor.d/usr.lib.libvirt.virt-aa-helper'
echo 'use ufw.sh to configure iptables'
echo https://docs.cloudstack.apache.org/en/latest/installguide/configuration.html#adding-a-host
echo 'from cloudstack install -- For KVM, do not put more than 16 hosts in a cluster'
echo 'If using a non-root user to add a KVM host, please add the user to sudoers file:'
echo 'cloudstack ALL=NOPASSWD: /usr/bin/cloudstack-setup-agent'
echo 'defaults:cloudstack !requiretty'

echo https://docs.cloudstack.apache.org/en/latest/installguide/configuration.html
echo setup region-zone-pod-cluster-host with primary and secondary storage
echo init and test ...

