#!/bin/bash
# https://artofcode.wordpress.com/2021/02/04/how-to-install-kvm-qemu-on-linux-mint-20-1/
echo apt install qemu-kvm libvirt-daemon-system libvirt-clients python3-libvirt bridge-utils virt-manager
echo adduser $USER libvirt
echo adduser $USER kvm
echo adduser $USER libvirt-qemu
echo virsh -c qemu:///system list
echo systemctl status libvirtd.service

echo suggested packages ...
echo ifupdown libosinfo-l10n gstreamer1.0-libav gstreamer1.0-plugins-bad auditd nfs-common open-iscsi pm-utils
echo systemtap zfsutils trousers python3-guestfs ssh-askpass python3-argcomplete xorriso-tcltk jigit cdck

