#!/bin/bash
echo https://libvirt.org/drvqemu.html#example-domain-xml-config
edho https://libvirt.org/drvqemu.html#example-domain-xml-config
echo https://www.cyberciti.biz/faq/create-vm-using-the-qcow2-image-file-in-kvm/
echo qemu-img create -f qcow2 -o preallocation=off vol128g 128G
echo virt-resize --quiet --expand /dev/sda1 vol.qcow newvol.qcow
echo wget https://download.fedoraproject.org/pub/fedora/linux/releases/40/Cloud/x86_64/images/Fedora-Cloud-Base-Generic.x86_64-40-1.14.qcow2
echo wget https://cloud-images.ubuntu.com/noble/current/noble-server-cloudimg-amd64.img -o ubu24.qcow

echo apt install qemu-system-x86-64 qemu-system-data
echo apt install libvirt-daemon libvirt-clients libguestfs-tools virt-what


